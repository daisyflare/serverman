package main

import (
	"flag"
	"os"
)

var home string
var server_cfg_path string
var user_cfg_dir string
var socket_loc string
var is_server bool

func init() {
	h, err := os.UserHomeDir()
	if err != nil {
		panic(err)
	}
	home = h
	cd, err := os.UserConfigDir()
	if err != nil {
		panic(err)
	}
	user_cfg_dir = cd
	flag.StringVar(&server_cfg_path, "config", user_cfg_dir + "/serverman/config.toml", "Path to the configuration file. Ignored if client.")

	flag.StringVar(&socket_loc, "socket", home + "/.local/share/serverman.socket", "Path to the socket to run the server with")

	flag.BoolVar(&is_server, "server", false, "Whether to start in client or server mode.")

	flag.Parse()
}

func main() {
	if is_server {
		runServer()
	} else {
		runClient()
	}
}
