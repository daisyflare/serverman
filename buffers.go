package main

type (
	RingBuf struct {
		Data []string
		End int
		Size int
		Looped bool
	}

	UpdateBuf struct {
		Senders map[int]chan string
	}
)

func NewUpdateBuf() *UpdateBuf {
	u := new(UpdateBuf)
	u.Senders = make(map[int]chan string)
	return u
}

func (u *UpdateBuf) PushAll(s string) {
	for _, c := range u.Senders {
		c <- s
	}
}

func (u *UpdateBuf) Write(p []byte) (n int, e error) {
	u.PushAll(string(p))
	return len(p), nil
}

func NewRingBuf(size int) *RingBuf {
	r := new(RingBuf)
	*r = RingBuf{
		Data: make([]string, size),
		End: 0,
		Looped: false,
		Size: size,
	}
	return r
}

func (r *RingBuf) Write(p []byte) (n int, e error) {
	s := string(p)
	r.Push(s)
	return len(p), nil
}

func (r *RingBuf) Push(s string) {
	r.Data[r.End] = s

	if r.Size == r.End + 1 {
		r.End = 0
		r.Looped = true
	} else {
		r.End++
	}
}

func (r *RingBuf) Flush() []string {
	if r.Looped && r.End + 1 < r.Size {
		return append(r.Data[r.End:], r.Data[:r.End]...)
	} else {
		t := make([]string, r.End)
		copy(t, r.Data[:r.End])
		return t
	}
}
