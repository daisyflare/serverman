package main

import (
	"bufio"
	"fmt"
	"io"
	"net/rpc"
	"os"

	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"
)

type (
	UI struct {
		view *tview.TextView
		app  *tview.Application
	}
)

var ui UI

func initUI() {
	app := tview.NewApplication()
	view := tview.NewTextView().
		SetChangedFunc(func() {
			app.Draw()
		})
	view.SetInputCapture(func(key *tcell.EventKey) *tcell.EventKey {
			if key.Key() == tcell.KeyRune && key.Rune() == 'q' || key.Rune() == 'Q' {
				app.Stop()
				os.Exit(0)
			}
			return key
		})
	ui = UI {
		view: view,
		app: app,
	}
}

func runClient() {
	go sigHandle(false)

	initUI()

	go recvClient(ui.view)

	ui.view.SetTitle("Serverman").
		SetBorder(true)

	if err := ui.app.SetRoot(ui.view, true).SetFocus(ui.view).Run(); err != nil {
		panic(err)
	}
}

func recvClient(rawW io.Writer) {
	w := bufio.NewWriter(rawW)

	var res Message

	client, err := rpc.DialHTTP("unix", socket_loc)
	if err != nil {
		panic(err)
	}

	for {
		err = client.Call("ServerMan.GetLog", EmptyArgs{}, &res)
		if err != nil {
			panic(err)
		}

		for msg := range res.Data {
			fmt.Fprint(w, res.Data[msg])
		}
		w.Flush()
	}
}
