// Sleeps 10 seconds, then echoes.
package main

import (
	"log"
	"os"
	"time"
)

func main() {
	log.SetOutput(os.Stdout)
	for {
		time.Sleep(10 * time.Second)

		log.Printf("Slept 10 seconds!\n")
	}
}
