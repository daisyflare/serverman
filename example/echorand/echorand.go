// Sleeps for a random number of seconds, then echoes how many seconds
// it slept for.
package main

import (
	"log"
	"math/rand"
	"os"
	"time"
)

const (
	MIN = 1
	MAX = 45
)

func main() {
	log.SetOutput(os.Stdout)

	rand.Seed(time.Now().UnixMilli())

	for {
		total_dur := (rand.Intn(MAX - MIN) + MIN)
		dur_left := total_dur
		dur := time.Second
		// This is really hacky but for some reason just doing time.Second * dur_left doesn't work,
		// so this has to be done
		for ; dur_left != 0; dur_left-- {
			dur += time.Second
		}

		time.Sleep(dur)

		log.Printf("Slept %d seconds!\n", total_dur)
	}
}
