package main

import (
	"log"
	"os"
	"os/signal"
	"syscall"
)

type (
	Message struct {
		Data []string
	}

	EmptyArgs struct {}

	ServerMan struct {}

	LogArgs struct {
		Pid int
	}
)

func (t *ServerMan) GetLog(args LogArgs, reply *Message) error {
	entry, exists := channeled_senders.Senders[args.Pid]

	if exists {
		messages := make([]string, 1)
		messages[0] = <- entry

		a: for {
			select {
			case msg := <- entry:
				messages = append(messages, msg)
				break
			default:
				break a
			}
		}
		*reply = Message {
			Data: messages,
		}
	} else {
		*reply = Message {
			Data: log_buf.Flush(),
		}
		channeled_senders.Senders[args.Pid] = make(chan string)
	}

	return nil
}

func sigHandle(is_serv bool) {
	c := make(chan os.Signal, 0)
	signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
	<- c
	if is_serv {
		os.Remove(socket_loc)
		log.Printf("Removed socket at %s!\n", socket_loc)
	}
	log.Fatal("Terminated by user...")
}
