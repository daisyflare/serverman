package main

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	"net/rpc"
	"os"
	"os/exec"
	"path"
	"reflect"
	"regexp"
	"strings"

	"github.com/pelletier/go-toml/v2"
)

type DataType int

const (
	DataOut DataType = iota
	DataErr
)

type (
	ServerConfig struct {
		Command string
	}

	Config struct {
		Path string
		Servers map[string]ServerConfig
	}

	SentData struct {
		Data string
		Name string
		Typ DataType
		Closed bool
	}
	SubServer struct {
		Stdout <-chan SentData
		Stderr <-chan SentData
		Stdin chan<- string
		Data *exec.Cmd
		Finished bool
		Code int
	}
)

func envOr(wanted, default_ string) string {
	val, ok := os.LookupEnv(wanted)
	if ok {
		return val;
	} else {
		return default_;
	}
}

var home_regex *regexp.Regexp

func replaceHome(src *string) {
	if home_regex == nil {
		home_regex = regexp.MustCompile(`(^|[^\\])(\\\\)*~`)
	}
	*src = home_regex.ReplaceAllString(*src, home)
}

func decodeFile(c *Config, p string) {
	if !path.IsAbs(p) {
		cwd, err := os.Getwd()
		if err != nil {
			log.Panic(err)
		}
		p = path.Join(cwd, p)
	}
	f, err := os.Open(p)
	if err != nil {
		panic(err)
	}
	d := toml.NewDecoder(f)
	d.DisallowUnknownFields()
	err = d.Decode(&c)
	if err != nil {
		panic(err)
	}
	if !path.IsAbs(c.Path) {
		dir, _ := path.Split(p)
		c.Path = path.Join(dir, c.Path)
	}
}

// Code taken from https://github.com/mitchellh/iochan
func channelReader(r io.Reader, name string, typ DataType) <-chan SentData {
	ch := make(chan SentData)

	go func() {
		buf := bufio.NewReader(r)
		for {
			res, err := buf.ReadString('\n')
			if errors.Is(err, io.EOF) {
				ch <- SentData {
					Name: name,
					Typ: typ,
					Closed: true,
					Data: "",
				}
				break;
			} else if err != nil {
				log.Fatalln(err)
			}
			ch <- SentData{
				Data: res,
				Name: name,
				Typ: typ,
				Closed: false,
			}
		}
	}()

	return ch
}

func channelWriter(w io.Writer) chan<- string {
	ch := make(chan string)
	go func() {
		buf := bufio.NewWriter(w)
		for to_write := range ch {
			buf.Write([]byte(to_write))
		}
	}()
	return ch
}

func isDir(p string) bool {
	info, err := os.Stat(p)
	if err != nil {
		var e *os.PathError
		if errors.As(err, &e) {
			return false;
		}
		panic(err)
	}
	return info.IsDir()
}

func makeServerOrElse(command string, name string) (serv SubServer, ok bool) {

	errorHandle := func(err error, typ string) {
		if err != nil {
			commandWords := strings.Split(command, " ")
			log.Fatalf("Error opening %s for command `%s %v`: %v!", typ, commandWords[0], commandWords[1:], err)
		}
	}

	cmd := exec.Command(command)

	se, err := cmd.StderrPipe()
	errorHandle(err, "stderr")
	serv.Stderr = channelReader(se, name, DataErr)

	so, err := cmd.StdoutPipe()
	errorHandle(err, "stderr")
	serv.Stdout = channelReader(so, name, DataOut)

	si, err := cmd.StdinPipe()
	errorHandle(err, "stdin")
	serv.Stdin = channelWriter(si)

	serv.Data = cmd

	if err := serv.Data.Start(); err != nil {
		log.Printf("ERROR: Could not start command `%v`: %v! Skipping!", command, err)
		return serv, false
	}

	return serv, true
}

func runAll(c *Config) (servs map[string]SubServer) {
	servs = make(map[string]SubServer)

	original_dir, err := os.Getwd()
	if err != nil {
		log.Panic(err)
	}

	var dir string
	if !isDir(c.Path) {
		dir, _ = path.Split(c.Path)
	} else {
		dir = c.Path
	}

	if err = os.Chdir(dir); err != nil {
		log.Fatalf("ERROR: Could not enter directory `%s` for config! Aborting!", path.Clean(dir))
	}

	for name, config := range c.Servers {
		cwd, err := os.Getwd()
		if err != nil {
			log.Panic(err)
		}

		if err = os.Chdir(name); err != nil {
			fmt.Println(dir)
			log.Printf("ERROR: Could not enter directory for %s! Skipping!\n", name)
			continue
		}
		s, ok := makeServerOrElse(config.Command, name)
		if !ok {
			_ = os.Chdir(cwd)
			continue
		}

		servs[name] = s

		_ = os.Chdir(cwd)
	}

	_ = os.Chdir(original_dir)

	return servs
}


func handleMsg(data SentData, servs *map[string]SubServer, pop *bool) bool {
	if data.Closed {
		*pop = true
		return false
	}

	rawMsg := re.ReplaceAll([]byte(data.Data), []byte(""))
	msg := strings.TrimSpace(string(rawMsg))

	whence := ""

	if data.Typ == DataErr {
		whence = " (stderr) "
	}

	log.Printf("[%s]%s: %s\n", data.Name, whence, msg)
	return false
}

var re *regexp.Regexp

// TODO: Add support for multiple clients
var log_buf = NewRingBuf(150)
var channeled_senders = NewUpdateBuf()

func runServer() {
	var config Config
	decodeFile(&config, server_cfg_path)

	go sigHandle(true)

	output := io.MultiWriter(log_buf, log.Default().Writer(), channeled_senders)

	log.SetOutput(output)

	log.Printf("Read config from path `%s`!\n", server_cfg_path)

	replaceHome(&config.Path)

	servs := runAll(&config)

	sm := new(ServerMan)
	rpc.Register(sm)
	rpc.HandleHTTP()

	listener, err := net.Listen("unix", socket_loc)
	if err != nil {
		panic(err)
	}
	defer os.Remove(socket_loc)
	go http.Serve(listener, nil)

	chans := make([]reflect.SelectCase, len(servs) * 2)

	idx := 0

	for _, data := range servs {
		c := reflect.SelectCase {
			Dir: reflect.SelectRecv,
			Chan: reflect.ValueOf(data.Stdout),
		}
		chans[idx] = c
		idx++

		c = reflect.SelectCase {
			Dir: reflect.SelectRecv,
			Chan: reflect.ValueOf(data.Stderr),
		}
		chans[idx] = c
		idx++
	}

	// Regexr link: regexr.com/791se
	re = regexp.MustCompile(`^(?:(?:(?:(?:\d{2,4}\/){2}\d{2,4})|(?:\d\d:\d\d:\d\d))\s)+`)
	for {
		idx, recvd, _ := reflect.Select(chans)
		data := recvd.Interface().(SentData)

		pop := false

		if handleMsg(data, &servs, &pop) {
			break
		}

		if pop {
			if len(chans) <= 2 {
				break
			}
			chans = append(chans[:idx], chans[idx+2:]...)

			go func() {
				entry := servs[data.Name]

				entry.Data.Wait()
				entry.Finished = true
				entry.Code = entry.Data.ProcessState.ExitCode()

				log.Printf("[Process %s exited with code %d]", data.Name, entry.Code)

				servs[data.Name] = entry
			}()
		}
	}
}
